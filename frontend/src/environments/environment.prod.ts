import {HttpHeaders} from "@angular/common/http";

export const environment = {
  production: true
};
export const API = {
  url: 'https://free-nba.p.rapidapi.com',
  headers: new HttpHeaders({
    'x-rapidapi-host': 'free-nba.p.rapidapi.com',
    'x-rapidapi-key': 'b00c930fd4mshc0b25cd56801598p1c4b49jsndfb1aaaef42b'
  })
}
