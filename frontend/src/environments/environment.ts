// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

import {HttpHeaders} from "@angular/common/http";

export const environment = {
  production: false
};

export const API = {
  url: 'https://free-nba.p.rapidapi.com',
  headers: new HttpHeaders({
    'x-rapidapi-host': 'free-nba.p.rapidapi.com',
    'x-rapidapi-key': 'b00c930fd4mshc0b25cd56801598p1c4b49jsndfb1aaaef42b'
  })
}

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
