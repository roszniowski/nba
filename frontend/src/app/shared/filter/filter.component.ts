import {Component, OnInit} from '@angular/core';

import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ResultsService} from "../../results.service";
import {ActivatedRoute, Params, Router} from "@angular/router";

import * as moment from "moment";

export interface Param {
  id?: number;
  season_from?: string;
  season_to?: string;
  team?: number;
}

export interface Team {
  id: number;
  full_name: string;
}

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  params: Param;
  filters: { season_from: string, season_to: string, id: string };
  filterGroup: FormGroup;
  teams: Array<Team>;
  teamLogo: number;
  minDate: Date;
  maxDate: Date;

  constructor(private resultsService: ResultsService, private route: ActivatedRoute, private router: Router) {
    const currentYear = new Date();
    this.minDate = new Date(currentYear.getFullYear() - 5, 0, 1);
    this.maxDate = new Date(currentYear);
    this.route.params.subscribe(
      (params: Params) => {
        this.params = params;
        this.teamLogo = params['id'];
      }
    )
    this.resultsService.getAllTeams().subscribe(
      (next) => {
        this.teams = next['data'];
      }
    )
    this.filterGroup = new FormGroup({
      season: new FormControl({
        begin: this.params.season_from? moment(this.params.season_from).format('YYYY') : moment,
        end: this.params.season_to ? moment(this.params.season_to).format('YYYY') : moment
      }),
      team: new FormControl(this.params.id ? +this.params.id : '', Validators.required)
    });
  }

  ngOnInit(): void {
  }

  filter() {
    this.filters = {
      season_from: moment(this.filterGroup.getRawValue().season.begin).format('YYYY'),
      season_to: moment(this.filterGroup.getRawValue().season.end).format('YYYY'),
      id: this.filterGroup.getRawValue().team
    }
    this.router.navigate([this.route.snapshot.data['path'],
      this.filters.id,
      this.filters.season_from,
      this.filters.season_to
    ])
  }
}
