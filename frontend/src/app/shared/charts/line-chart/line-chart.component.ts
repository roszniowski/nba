import {Component} from '@angular/core';
import {ActivatedRoute, Data, Params} from "@angular/router";
import {ResultsService} from "../../../results.service";
import * as moment from 'moment';

@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.scss']
})
export class LineChartComponent {
  chartType: string = 'line';
  chartDatasets: Array<any> = [];
  chartLabels: Array<any> = [];
  chartColors: Array<any> = [
    {
      backgroundColor: 'rgba(105, 0, 132, .2)',
      borderColor: 'rgba(200, 99, 132, .7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(0, 137, 132, .2)',
      borderColor: 'rgba(0, 10, 130, .7)',
      borderWidth: 2,
    }
  ];
  chartOptions: any = {
    responsive: true
  };
  chartReady = false;
  isLoadingResults = true;

  constructor(private route: ActivatedRoute, private resultsService: ResultsService) {
    this.route.params.subscribe(
      (params: Params) => {
        this.chartReady = false;
        this.isLoadingResults = true;
        this.resultsService.getAllGames(params).subscribe(
          (data: Data) => {
            let scored = [];
            let lost = [];
            this.chartLabels = [];
            this.chartDatasets = [];
            let chartsData = data['data'].map(
              item => ({
                date: item.date,
                points_scored: +params['id'] === item.home_team.id ? item.home_team_score : item.visitor_team_score,
                points_lost: +params['id'] === item.home_team.id ? item.visitor_team_score : item.home_team_score,
              })
            );
            chartsData.forEach((elm) => {
              let date = moment(elm.date).format('YYYY-MM-DD');
              scored.push(elm.points_scored);
              lost.push(elm.points_lost);
              this.chartLabels.push(date);
            })
            this.chartDatasets.push({data: scored, label: 'Points scored'}, {data: lost, label: 'Points lost'});
            this.chartReady = true;
            this.isLoadingResults = false;
          }
        )
      }
    );
  }
}
