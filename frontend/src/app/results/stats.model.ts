export interface Stat {
  game_id: number;
  season: string | Date;
  points_scored: number;
  points_lost: number;
  date: string | Date;
  team_id: number;
  name: string;
  division: string;
  conference: string;
  rebounds: number;
  assists: number;
}
