import {AfterViewInit, Component, ViewChild} from '@angular/core';
import {ResultsService} from "../results.service";
import {ActivatedRoute, Params} from "@angular/router";
import {MatTableDataSource} from "@angular/material/table";
import {MatSort} from "@angular/material/sort";
import {Stat} from "./stats.model";
import {groupBy, map, mergeMap, toArray} from "rxjs/operators";
import {from} from "rxjs";
import {MatPaginator} from "@angular/material/paginator";
import {MatSnackBar} from "@angular/material/snack-bar";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements AfterViewInit {
  @ViewChild(MatSort, {static: true}) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['season', 'opponent_team', 'result', 'points_scored', 'points_lost', 'rebounds', 'assists', 'date', 'opponent_div', 'opponent_conf'];
  dataSource: MatTableDataSource<Stat>;
  data: Stat;
  stats;
  resultsLength = 0;
  isLoadingResults = true;

  constructor(private resultsService: ResultsService,
              private route: ActivatedRoute,
              private _snackBar: MatSnackBar) {
  }

  ngAfterViewInit() {
    this.route.params.subscribe(
      (params: Params) => {
        if (!params) return;
        this.isLoadingResults = true;
        this.resultsService.getAllGames(params).subscribe(
          (data) => {
            let ids = [];
            this.resultsLength = data['meta'].total_count;
            this.stats = data['data'];
            this.stats.forEach((stat) => {
              ids.push(stat.id);
            })
            this.resultsService.getStatsByGame(ids).subscribe(
              (data) => {
                let results = data['data'].map(item => ({
                  game_id: item.game.id,
                  rebounds: item.reb,
                  assists: item.ast
                }));
                from(results).pipe(
                  groupBy(val => val['game_id']),
                  mergeMap(group => {
                    return group.pipe(toArray());
                  }),
                  map((val) => {
                    let rebounds = 0;
                    let assists = 0
                    val.map(v => {
                      rebounds = rebounds + v['rebounds'];
                      assists = assists + v['assists'];
                    });
                    return {
                      game: val[0]['game_id'],
                      rebounds: rebounds,
                      assists: assists
                    };
                  }),
                  toArray()
                ).subscribe(
                  val => {
                    let results = this.stats.map(item => ({
                      game_id: item.id,
                      season: item.season,
                      points_scored: +params['id'] === item.home_team.id ? item.home_team_score : item.visitor_team_score,
                      points_lost: +params['id'] === item.home_team.id ? item.visitor_team_score : item.home_team_score,
                      date: item.date,
                      team_id: +params['id'] === item.home_team.id ? item.visitor_team.id : item.home_team.id,
                      name: +params['id'] === item.home_team.id ? item.visitor_team.name : item.home_team.name,
                      division: +params['id'] === item.home_team.id ? item.visitor_team.division : item.home_team.division,
                      conference: +params['id'] === item.home_team.id ? item.visitor_team.conference : item.home_team.conference
                    }));
                    val.forEach((value) => {
                      results.forEach((result) => {
                        if (value.game === result.game_id) {
                          result.rebounds = value.rebounds;
                          result.assists = value.assists;
                        }
                      })
                    })
                    this.dataSource = new MatTableDataSource<Stat>(results);
                    this.dataSource.paginator = this.paginator;
                    this.dataSource.sort = this.sort;
                    this.isLoadingResults = false;
                  }
                )
              },
              (error) => {
                this.openSnackBar(error);
                this.isLoadingResults = false;
              }
            )
          },
          (error) => {
            this.openSnackBar(error);
            this.isLoadingResults = false;
          }
        )
      }
    )
    this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
  }

  openSnackBar(message: string) {
    this._snackBar.open(message,'', {
      duration: 2000,
    });
  }
}
