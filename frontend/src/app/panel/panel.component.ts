import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import * as moment from "moment";
import {ResultsService} from "../results.service";

@Component({
  selector: 'app-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {
  teams: Array<any>;
  teamsForm = new FormGroup({
    team_id: new FormControl('', Validators.required),
    season: new FormControl('', Validators.required)
  });
  params: { id: number, season_from: string, season_to: string };
  minDate: Date;
  maxDate: Date;

  constructor(private resultsService: ResultsService, private router: Router) {
    const currentYear = new Date();
    this.minDate = new Date(currentYear.getFullYear() - 5, 0, 1);
    this.maxDate = new Date(currentYear);
    this.resultsService.getAllTeams().subscribe(
      (next) => {
        this.teams = next['data'];
      }
    )
  }

  ngOnInit(): void {
  }

  onSubmit() {
    this.params = {
      id: this.teamsForm.getRawValue().team_id,
      season_from: moment(this.teamsForm.getRawValue().season.begin).format('YYYY'),
      season_to: moment(this.teamsForm.getRawValue().season.end).format('YYYY')
    }
    this.router.navigate(['/results/',
      this.params.id,
      this.params.season_from,
      this.params.season_to
    ])
  }

}
