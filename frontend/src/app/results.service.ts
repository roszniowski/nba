import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from "@angular/common/http";
import {API} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ResultsService {

  constructor(private http: HttpClient) {
  }

  getStatsByGame(ids: Array<number>) {
    let httpParams = new HttpParams();
    httpParams = httpParams.append('per_page', '100');
    if(!ids) return;
    ids.forEach((id) => {
      httpParams = httpParams.append('game_ids[]', id.toString());
    })
    return this.http.get(API.url + '/stats', {
      headers: API.headers,
      params: httpParams
    });
  }

  getAllGames(params?) {
    let httpParams = new HttpParams();
    if(params['id']) httpParams = httpParams.append('team_ids[]', params['id']);
    if(params['season_from']) httpParams = httpParams.append('seasons[]', params['season_from']);
    if(params['season_to'])httpParams = httpParams.append('seasons[]', params['season_to']);
    httpParams = httpParams.append('per_page', '100');
    return this.http.get(API.url + '/games', {
      headers: API.headers,
      params: httpParams
    });
  }

  getAllTeams() {
    return this.http.get(API.url + '/teams', {
      headers: API.headers
    });
  }
}
