import { Pipe, PipeTransform } from '@angular/core';

const idToLogoMapper = {
  1: 'hawks',
  2: 'celtics',
  3: 'nets',
  4: 'hornets',
  5: 'bulls',
  6: 'cavaliers',
  7: 'mavericks',
  8: 'nuggets',
  9: 'pistons',
  10: 'warriors',
  11: 'rockets',
  12: 'pacers',
  13: 'clippers',
  14: 'lakers',
  15: 'grizzlies',
  16: 'heat',
  17: 'bucks',
  18: 'timberwolves',
  19: 'pelicans',
  20: 'knicks',
  21: 'thunder',
  22: 'magic',
  23: '76ers',
  24: 'suns',
  25: 'blazers',
  26: 'kings',
  27: 'spurs',
  28: 'raptors',
  29: 'jazz',
  30: 'wizards'
}

@Pipe({
  name: 'teamLogoRenderer'
})
export class TeamLogoRendererPipe implements PipeTransform {

  transform(id) {
    return idToLogoMapper[id];
  }

}
