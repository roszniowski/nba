import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ResultsComponent} from "./results/results.component";
import {PanelComponent} from "./panel/panel.component";
import {LineChartComponent} from "./shared/charts/line-chart/line-chart.component";
import {PageNotFoundComponent} from "./page-not-found/page-not-found.component";


const routes: Routes = [
  {path: 'results', component: ResultsComponent, data: {animation: 'ResultsPage', path: 'results'}},
  {path: 'results/:id/:season_from/:season_to', component: ResultsComponent, data: {animation: 'ResultsPage', path: 'results'}},
  {path: 'results/:season_from/:season_to', component: ResultsComponent, data: {animation: 'ResultsPage', path: 'results'}},
  {path: 'charts', component: LineChartComponent, data: {animation: 'ChartsPage', path: 'charts'}},
  {path: 'charts/:id/:season_from/:season_to', component: LineChartComponent, data: {animation: 'ChartsPage', path: 'charts'}},
  {path: 'home', component: PanelComponent, data: {animation: 'HomePage'}},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
