import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ResultsComponent} from './results/results.component';
import {FilterComponent} from './shared/filter/filter.component';
import {LineChartComponent} from './shared/charts/line-chart/line-chart.component';
import {NavbarComponent} from './navbar/navbar.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {MatNativeDateModule} from "@angular/material/core";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MaterialModule} from "./material.module";
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from "saturn-datepicker";
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from "@angular/material-moment-adapter";
import {HttpClientModule} from "@angular/common/http";
import {PanelComponent} from './panel/panel.component';
import { TeamLogoRendererPipe } from './team-logo-renderer.pipe';
import {MDBBootstrapModule} from "angular-bootstrap-md";
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    ResultsComponent,
    FilterComponent,
    LineChartComponent,
    NavbarComponent,
    PanelComponent,
    TeamLogoRendererPipe,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    MatNativeDateModule,
    MaterialModule,
    HttpClientModule,
    MDBBootstrapModule,
  ],
  providers: [
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: {
        parse: {
          dateInput: 'LL',
        },
        display: {
          dateInput: 'YYYY',
          monthYearLabel: 'YYYY',
          dateA11yLabel: 'LL',
          monthYearA11yLabel: 'YYYY',
        },
      }},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
